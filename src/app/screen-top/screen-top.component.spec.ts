import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScreenTopComponent } from './screen-top.component';

describe('ScreenTopComponent', () => {
  let component: ScreenTopComponent;
  let fixture: ComponentFixture<ScreenTopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScreenTopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScreenTopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
