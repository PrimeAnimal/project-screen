import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScreenBottomComponent } from './screen-bottom.component';

describe('ScreenBottomComponent', () => {
  let component: ScreenBottomComponent;
  let fixture: ComponentFixture<ScreenBottomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScreenBottomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScreenBottomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
