import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class ProjectsService {
    
    private projectsUrl = 'https://europe-west1-kynda-one.cloudfunctions.net/api/planning';  // URL to agenda api    
    private quoteotdsUrl = 'https://europe-west1-kynda-one.cloudfunctions.net/api/quote';     // URL to quote api
    private footballsUrl = 'https://europe-west1-kynda-one.cloudfunctions.net/api/fussball';  // URL to football api 

    constructor(private http: HttpClient) {
    }

    getProjects(): Observable<any> {
        return this.http.get<any>(this.projectsUrl);
    }
    getquoteotd(): Observable<any> {
        return this.http.get<any>(this.quoteotdsUrl);
    }
    getfootball(): Observable<any> {
        return this.http.get<any>(this.footballsUrl);
    }
}