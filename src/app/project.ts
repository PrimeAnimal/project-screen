export class Project {
    data: {
        Opdrachtgever: string;
        Project: string;
        Account: string;
        Status: any;
        Status2: string;
        "Next milestone": any;
        "Milestone actie": string;
        Deadline: any;
        "Proces gestart vanaf": any;
        Webflow: any;
    }
}
export class quoteotd{
    data: {
        quote1: any;
        quote2: any;
    }
}
export class football{
    data: {
        persoon: string;
        kruipen: number;
    }
}