import { Component, OnInit, Input } from '@angular/core';
import { ProjectsService } from './../project.service';
import { quoteotd } from './../project'

@Component({
  selector: 'app-quote',
  templateUrl: './quote.component.html',
  styleUrls: ['./quote.component.scss']
})
export class QuoteComponent implements OnInit {
  rawData = [];
  quoteotds: quoteotd[] = [];
  interval;
  quoteotd = [];
  @Input() screenquote = false;

  constructor(private QuoteotdService: ProjectsService) {

  }

  ngOnInit() {
    this.getquoteotd();
    this.resetQuote();
  }
  // gets the quote information
  getquoteotd(): void {
    this.QuoteotdService.getquoteotd().subscribe(
      (quoteotds) => {
        this.rawData = quoteotds.slice(1);
        this.quote();
      }, (error) => {
        console.log("couldn't find the data server", error)
      });
  }

  // this is for the quote on the last slide (based on time it will change)
  // quote[0] top part
  // quote[1] bottom part
  quote() {
    for (const quoteotd of this.rawData) {
      const time = new Date().getMinutes();
      if (time <= 14 && quoteotd[2] == 1 || time > 59 && quoteotd[2] == 1) {
        this.quoteotd.push(quoteotd);
      }
      else if (time <= 29 && quoteotd[2] == 2) {
        this.quoteotd.push(quoteotd);
      }
      else if (time <= 44 && quoteotd[2] == 3) {
        this.quoteotd.push(quoteotd);
      }
      else if (time <= 59 && quoteotd[2] == 4) {
        this.quoteotd.push(quoteotd);
      }
    }
  }

  //  this picks up the data of the API every 1 hour and checks every 10 seconds for the minutes of the quote
  resetQuote() {
    setInterval(() => {
      this.quote();
    }, 10000);
    setInterval(() => {
      this.quoteotd = [];
      this.getquoteotd();
    }, 3600000);
  }

  ngOnDestroy() {
    // destroy the interval to clear the javascript heap
    this.interval.clear();
  }
}
