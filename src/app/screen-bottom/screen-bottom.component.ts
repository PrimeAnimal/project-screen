import { Component, OnInit } from '@angular/core';
import { ProjectsService } from './../project.service';
import { football } from './../project';

@Component({
  selector: 'app-screen-bottom',
  templateUrl: './screen-bottom.component.html',
  styleUrls: ['./screen-bottom.component.scss']
})
export class ScreenBottomComponent implements OnInit {
  interval;
  rawData = [];
  footballs: football[] = [];
  crawl = [];
  // checks if the browser is chrome
  isChrome = /Chrome/i.test(window.navigator.userAgent);

  constructor(private FootballService: ProjectsService) {
  }

  ngOnInit() {
    this.getfootball();
    this.resetCrawled();
  }

  // gets the football information
  getfootball(): void {
    this.FootballService.getfootball().subscribe(
      (footballs) => {
        this.rawData = footballs.slice(1);
        this.showCrawled();
      }, (error) => {
        console.log("couldn't find the data server", error)
      });
  }

  // this sorts out the persons first on name and then on the amount of times they needed to crawl 
  showCrawled() {
    for (const info of this.rawData) {
      // info[0] = person
      // info[1] = crawled
      if (this.isChrome === true) {
        this.crawl.push(info);
        this.crawl = this.crawl.sort((a, b) => (a[0] > b[0]) ? 1 : -1);
        this.crawl = this.crawl.sort((a, b) => (a[1] > b[1]) ? -1 : 1);
      }
      else {
        this.crawl.push(info);
        this.crawl = this.crawl.sort((a, b) => (a[0] > b[0]) ? -1 : 1);
        this.crawl = this.crawl.sort((a, b) => (a[1] > b[1]) ? -1 : 1);
      }
    }
  }

  //  this picks up the data of the API every 1 hour
  resetCrawled() {
    setInterval(() => {
      this.crawl = [];
      this.getfootball();
    }, 3600000);
  }
  ngOnDestroy() {
    // destroy the interval to clear the javascript heap
    this.interval.clear();
  }
}
