import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ScreenComponent } from './screen/screen.component';
import { QuoteComponent } from './quote/quote.component';
import { ScreenTopComponent } from './screen-top/screen-top.component';


const routes: Routes = [
  {path: 'screen', component: ScreenComponent},
  {path: 'quote', component: QuoteComponent},
  {path: '', component: ScreenTopComponent, outlet:'top' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }


