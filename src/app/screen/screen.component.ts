import { Component, OnInit } from '@angular/core';
import { Project } from "./../project";
import { ProjectsService } from "./../project.service";
import { tns } from "tiny-slider/src/tiny-slider";

@Component({
  selector: 'app-screen',
  templateUrl: './screen.component.html',
  styleUrls: ['./screen.component.scss']
})

export class ScreenComponent implements OnInit {

  rawData = [];
  projects: Project[] = [];
  mileS = [];
  deadL = [];
  noTime = [];
  now;
  milestoneSize = false;
  today;
  yesterday;
  twoWeeks;
  interval;
  slider;
  milestoneS = true;
  isChrome = /Chrome/i.test(window.navigator.userAgent);

  constructor(private projectsService: ProjectsService) {
  }

  ngOnInit() {
    // tiny-slider used from npm
    this.slider = tns({
      container: '.my-slider',
      items: 1,
      slideBy: 'page',
      autoplay: true,
      controls: false,
      nav: false,
      speed: 0,
      autoplayTimeout: 30000,
      autoplayButtonOutput: false,
      mouseDrag: true,
    });
    this.getProjects();
    this.resetProject();
  }

  // gets the api's information 
  getProjects(): void {
    this.projectsService.getProjects().subscribe(
      (projects) => {
        this.rawData = projects.slice(1);
        this.showdata();
      }, (error) => {
        console.log("couldn't find the data server", error)
      });
  }

  // makes a date for the api that understands it (year in 0000 - month in 00 - day in 00)
  dateToDate() {
    this.now = new Date();
    this.today = this.now.getFullYear() + '-' + ("0" + (this.now.getMonth() + 1)).slice(-2) + '-' + this.now.getDate();
    if(this.now.getDate() <= 10){
      this.yesterday = this.now.getFullYear() + '-' + ("0" + (this.now.getMonth() + 1)).slice(-2) + '-0' + (this.now.getDate() - 1);
    }
    else if(this.now.getDate() > 10){
      this.yesterday = this.now.getFullYear() + '-' + ("0" + (this.now.getMonth() + 1)).slice(-2) + '-' + (this.now.getDate() - 1);
    }
  }

  showdata() {
    for (const project of this.rawData) {
      // "project[2]" = person 
      // "project[5]" = Milestone 
      // "project[6]" = status (onscreen shown)
      // "project[7]" = Deadline 
      this.dateToDate();
      // puts in its own value if its shorter then x provided charathers and adds true or false based on text
      if (!!project[2] && project[6] == undefined || !!project[2] && project[6] == !!null) {
        project[6] = '';
        project.type = false;
      }
      else if (Array.from(project[6]).length <= 1) {
        project[6] = '';
        project.type = false;
      }
      else if (Array.from(project[6]).length > 1) {
        project.type = true;
      }
      if (!!project[2] && project[6] == '') {
        project[6] = 'Nog in te vullen!';
      }
      // makes the no date here before everything else cause of removal from project[5] and project[7] later and sorts it if it has texts then puts that first
      if (!!project[2] && !project[5] && !project[7]) {
        this.noTime.push(project);
        this.noTime = this.noTime.sort((a, b) => (a.type > b.type) ? -1 : 1);
      }
      // removes if the time is not a number/undefined
      if (!!project[5] && !!project[2] || !!project[7] && !!project[2]) {
        if (project[5] == undefined) {
          project[5] = '';
        }
        if (project[7] == undefined) {
          project[7] = '';
        }
        // puts the milestone to the deadline IF it has a deadline 
        if (!!project[5]) {
          if (project[5] <= this.today && !!project[7]) {
            project[5] = '';
          }
          // gives property False or True on time based for opacity
          project.forEach(() => {
            if (project[5] >= this.yesterday) {
              project.datePassed = false;
            }
            if (project[5] < this.yesterday) {
              project.datePassed = true;
            }
          });
        }
        // gives property 
        if (!!project[7]) {
          project.forEach(() => {
            if (project[7] >= this.yesterday) {
              project.datePassed = false;
            }
            else if (project[7] < this.yesterday) {
              project.datePassed = true;
            }
          });
        }
      }
      // sort function to order on time based and if it has text
      if (!!project[7] && !project[5]) {
        if (this.isChrome == true) {
          this.deadL.push(project);
          this.deadL = this.deadL.sort((a, b) => (a.type > b.type) ? 1 : -1);
          this.deadL = this.deadL.sort((a, b) => (a[7] > b[7]) ? 1 : -1);
        }
        else {
          this.deadL.push(project);
          this.deadL = this.deadL.sort((a, b) => (a.type > b.type) ? -1 : 1);
          this.deadL = this.deadL.sort((a, b) => (a[7] > b[7]) ? 1 : -1);
        }
      }
      if (!!project[5]) {
        if (this.isChrome == true) {
          this.mileS.push(project);
          this.mileS = this.mileS.sort((a, b) => (a.type > b.type) ? 1 : -1);
          this.mileS = this.mileS.sort((a, b) => (a[5] > b[5]) ? 1 : -1);
        }
        else {
          this.mileS.push(project);
          this.mileS = this.mileS.sort((a, b) => (a.type > b.type) ? -1 : 1);
          this.mileS = this.mileS.sort((a, b) => (a[5] > b[5]) ? 1 : -1);
        }
      }
    }
  }

  // this picks up the data of the API every 1 hour
  resetProject() {
    setInterval(() => {
      this.rawData = [];
      this.mileS = [];
      this.deadL = [];
      this.noTime = [];
      this.now = new Date();
      this.getProjects();
    }, 3600000);
  }

  ngOnDestroy() {
    // destroy the interval to clear the javascript heap
    this.interval.clear();
  }
}