import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ScreenComponent } from './screen/screen.component';
import { HttpClientModule } from '@angular/common/http';
import { ScreenTopComponent } from './screen-top/screen-top.component';
import { ScreenBottomComponent } from './screen-bottom/screen-bottom.component';
import { QuoteComponent } from './quote/quote.component';

@NgModule({
  declarations: [
    AppComponent,
    ScreenComponent,
    ScreenTopComponent,
    ScreenBottomComponent,
    QuoteComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { };


