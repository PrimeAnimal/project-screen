import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-screen-top',
  templateUrl: './screen-top.component.html',
  styleUrls: ['./screen-top.component.scss']
})
export class ScreenTopComponent implements OnInit {
  now = new Date();
  interval;

  ngOnInit() {
    this.timer();
  }

  // this is for the clock 
  timer() {
    this.interval = setInterval(() => {
      this.now = new Date();
    }, 10000);
  }

  ngOnDestroy() {
    // destroy the interval to clear the javascript heap
    this.interval.clear();
  }
}
